import { Component } from '@angular/core';
import { AuthService } from '../servicios/auth.service';
import { ChatsService, chat } from '../servicios/chats.service';
import { ModalController } from '@ionic/angular';
import { ChatComponent } from "../componentes/chat/chat.component";
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public chatRooms : any = [];

  constructor(private authService : AuthService, private chatService: ChatsService, private modal : ModalController, public actionSheetController: ActionSheetController) {}

  onLogOut(){
    this.authService.logOut();
  }

  ngOnInit(){
    this.chatService.getChatRooms().subscribe( chats => {
      
      this.chatRooms = chats;

    });
  }

  openChat(chat){
    this.modal.create({
      component: ChatComponent,
      componentProps: {
        chat: chat
      }
    }).then((modal) =>{
      modal.present();
    })
  }
  
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Cerrar Sesion',
        icon: 'log-out',
        handler: () => {
          this.authService.logOut();
        }
      },
      {
        text: 'Perfil',
        icon: 'person-circle-outline',
        handler: () => {
          console.log("aqui redirige a la pagina del usuario");
        }
      }]
    });
    await actionSheet.present();
  }

}
