import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'
import { Router } from '@angular/router';
import { auth } from 'firebase';
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private AFauth: AngularFireAuth, public router: Router, private db : AngularFirestore) { }

  login(email: string, password: string) {

    return new Promise((resolve, rejected) => {
      this.AFauth.signInWithEmailAndPassword(email, password).then(user => {
        resolve(user);
      }).catch(err => {
        rejected(err);
      })
    });
  }

  logOut() {
    this.AFauth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }

  register(email: string, password: string, name: string) {
    return new Promise((resolve, rejected) => {
      this.AFauth.createUserWithEmailAndPassword(email, password).then(res =>{

        const uid = res.user.uid;

        this.db.collection('users').doc(res.user.uid).set({
          name: name,
          uid: res.user.uid,
          ProfileIMG: ""
        })

        resolve(res)
      }).catch(err => rejected(err))
    })
  }
}
